package oma.intern.ussd.screen;

import com.google.gson.Gson;
import org.junit.Test;

/*
 *Date : 17/08/2020 17:08:2020
 *User : faf_ext
 */
public class UssdResponseTest {

    @Test
    public void testToString(){
        USSDResponse response = new USSDResponse();

        response.setBody("Test Body");
        response.setEnd(false);
        response.setFormAction("http://url-form");
        response.setLibMenu("home");
        response.setLibNext("next");
        response.setLibReturn("return");
        response.setReturnMenu("http://return-url");
        response.setTitle("titla");

        response.getParameter().put("lang", "en");
        response.getNextMenu().put("2", "Details");

        System.out.println(new Gson().toJson(response));
    }
}
